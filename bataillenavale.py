import random

# Création de la grille de jeu en 15x15 et de la grille de placement de bateaux
grille = [[" " for _ in range(15)] for _ in range(15)]
grillebateaux = [[" " for _ in range(15)] for _ in range(15)]

# Fonction pour afficher la grille de jeu
def afficher_grille(grille):
    print("  1 2 3 4 5 6 7 8 9 10 11 12 13 14 15")
    for i in range(15):
        ligne = str(i + 1) + " "
        for j in range(15):
            ligne += grille[i][j] + " "
        print(ligne)

# Fonction pour placer des bateaux de taille variable sur la grille bateaux
def placer_bateaux(grillebateaux, taille_bateaux):
    for taille in taille_bateaux:
        placer_bateau(grillebateaux, taille)

# Fonction pour placer un bateau de taille donnée sur la grille bateaux
def placer_bateau(grillebateaux, taille):

    taille_grille = len(grillebateaux)
    direction = random.choice(['horizontal', 'vertical'])

    if direction == 'horizontal':
        x = random.randint(0, taille_grille - 1)
        y = random.randint(0, taille_grille - taille)
        #Cette ligne permet de vérifier les voisins autour
        #if grillebateaux[x][y] == " " and grillebateaux[x-1][y+1] != 'B' and grillebateaux[x][y+1] != 'B' and grillebateaux[x+1][y+1] != 'B' and grillebateaux[x-1][y] != 'B' and grillebateaux[x+1][y] != 'B' and grillebateaux[x-1][y-1] != 'B' and grillebateaux[x][y-1] != 'B' and grillebateaux[x+1][y-1] != 'B':
        if grillebateaux[x][y] == " ":
            for i in range(taille):
                grillebateaux[x][y + i] = 'B'
        else :
             placer_bateau(grillebateaux, taille)
    else:
        x = random.randint(0, taille_grille - taille)
        y = random.randint(0, taille_grille - 1)
        #Cette ligne permet de vérifier les voisins autour
        #if grillebateaux[x][y] == " " and grillebateaux[x-1][y+1] != 'B' and grillebateaux[x][y+1] != 'B' and grillebateaux[x+1][y+1] != 'B' and grillebateaux[x-1][y] != 'B' and grillebateaux[x+1][y] != 'B' and grillebateaux[x-1][y-1] != 'B' and grillebateaux[x][y-1] != 'B' and grillebateaux[x+1][y-1] != 'B':
        if grillebateaux[x][y] == " ":
            for i in range(taille):
                grillebateaux[x + i][y] = 'B'
        else :
            placer_bateau(grillebateaux, taille)

# Programme principal
def jeu_bataille_navale(taille_bateaux):
    print("Bienvenue dans le jeu de bataille navale !")
    placer_bateaux(grillebateaux, taille_bateaux)
    afficher_grille(grille)
    #afficher_grille(grillebateaux) #affiche la grille de solution

    while True:
        ligne = int(input("Entrez le numéro de ligne : ")) - 1
        colonne = int(input("Entrez le numéro de colonne : ")) -1

        if ligne < 0 or ligne > 14 or colonne < 0 or colonne > 14:
            print("Coordonnées hors de la grille. Réessayez.")
            continue

        elif grillebateaux[ligne][colonne] == 'B':
            print("Touché !")
            grille[ligne][colonne] = 'X'
            grillebateaux[ligne][colonne] = 'X'

            if 'B' not in [cell for row in grillebateaux for cell in row]:
                afficher_grille(grille)
                print("Vous avez gagné !")
                break

        elif grillebateaux[ligne][colonne] == 'X':
            print("Vous avez déjà tiré ici.")
        else:
            print("Manqué.")
            grille[ligne][colonne] = '-'

        afficher_grille(grille)

# Taille des bateaux
taille_bateaux = [1, 1, 2, 2, 3, 3, 4]

jeu_bataille_navale(taille_bateaux)
